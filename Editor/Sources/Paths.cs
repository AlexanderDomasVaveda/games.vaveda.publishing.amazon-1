﻿// Copyright (C) 2020 Vaveda Games (https://vaveda.games) - All Rights Reserved.
// Unauthorized copying of this file, via any medium is strictly prohibited.
// Proprietary and confidential.

using Vaveda.Integration.Scripts.Utils;

namespace Vaveda.Integration.Sources
{
    internal static class Paths
    {
        internal static readonly string PackageName =
            Defines.IsAmazon ? "games.vaveda.publishing.amazon" : "games.vaveda.publishing";

        internal static class Files
        {
            public const string Linker = "link.xml";
        }

        internal static class Configs
        {
            public const string AssetsFolder = "Assets";
            public const string ConfigsFolder = "Configs";
            public const string Packages = "Packages";
            public const string ResourcesFolder = "Resources";
            public const string StreamingAssetsFolder = "StreamingAssets";
        }
    }
}