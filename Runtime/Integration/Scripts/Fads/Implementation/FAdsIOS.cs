﻿using System;
using System.Runtime.InteropServices;
using UnityEngine;

namespace FAdsSdk
{
#if UNITY_IOS
    public class FAdsIOS : FAdsBase
    {
        static FAdsIOS()
        {
            InitManager();
        }

        #region API

        public static void InitializeConfig(string cfg, string key)
        {
            FAdsInitializeConfig(cfg, key, 0);
        }

        public static void SetBannerPosition(BannerPosition bannerPosition, float screenOffset)
        {
            int bPos = 0;
            switch (bannerPosition)
            {
                case BannerPosition.Bottom:
                    bPos = 0;
                    break;
                case BannerPosition.Top:
                    bPos = 1;
                    break;
            }

            FAdsUnitySetBannerPosition(bPos, screenOffset);
        }

        public static void InitializeSdk(bool limitTracking, bool ccpaApplies)
        {
            FAdsUnityInitialize();
            FAdsSetCCPAOptOut(limitTracking);
            FAdsSetCCPAApply(ccpaApplies);
        }

        public static void ShowBanner(string placement, string tag)
        {
            FAdsBannerShow(placement, tag);
        }

        public static void HideBanner()
        {
            FAdsBannerHide();
        }

        public static int InterstitialReadyState()
        {
            return FAdsInterstitialReadyState();
        }

        public static void ShowInterstitial(string placement, string tag)
        {
            FAdsInterstitialShow(placement, tag);
        }

        public static void LoadRewarded()
        {
            FAdsEnableRewarded(null, true);
        }

        public static int RewardedReadyState()
        {
            return FAdsRewardedReadyState(null);
        }

        public static void ShowRewarded(string placement, string tag)
        {
            FAdsRewardedShow(null, placement, tag);
        }

        public static void DisableAds(AdShowType cacheType)
        {
            if ((cacheType & AdShowType.Banner) == AdShowType.Banner)
            {
                FAdsEnableBanner(false);
            }

            if ((cacheType & AdShowType.Interstitial) == AdShowType.Interstitial)
            {
                FAdsEnableInterstitial(false);
            }

            if ((cacheType & AdShowType.Rewarded) == AdShowType.Rewarded)
            {
                FAdsEnableRewarded(null, false);
            }
        }

        public static void EnableAds(AdShowType cacheType)
        {
            if ((cacheType & AdShowType.Banner) == AdShowType.Banner)
            {
                FAdsEnableBanner(true);
            }

            if ((cacheType & AdShowType.Interstitial) == AdShowType.Interstitial)
            {
                FAdsEnableInterstitial(true);
            }

            if ((cacheType & AdShowType.Rewarded) == AdShowType.Rewarded)
            {
                FAdsEnableRewarded(null, true);
            }
        }


        public static void GrantConsent()
        {
            FAdsGrantConsent();
        }

        public static bool ShouldShowConsentDialog()
        {
            return FAdsIsNeedShowConsent();
        }

        public static int IsGDPRApplicable()
        {
            return FAdsIsGDPRApplicable();
        }

        public static string GetConnectionType()
        {
            return FAdsUnityGetConnection();
        }

        public static string GetStateLog()
        {
            return null;
        }

        public static bool IsTablet()
        {
            return isTabletAdLayout();
        }

        public static void AllowPauseRenderer(bool allow)
        {
            FAdsAllowPauseRender(allow);
        }

        public static string GetNativeVersion()
        {
            return FAdsUnityGetNativeVersion();
        }

        #endregion

        #region mapping

        #region wrapper

        [DllImport("__Internal")]
        private static extern void FAdsUnityInitialize();

        // 0 - bottom, 1 - top
        [DllImport("__Internal")]
        private static extern void FAdsUnitySetBannerPosition(int pos, float screenOffset);

        [DllImport("__Internal")]
        private static extern string FAdsUnityGetConnection();

        [DllImport("__Internal")]
        private static extern string FAdsUnityGetNativeVersion();

        #endregion

        #region fads

        [DllImport("__Internal")]
        private static extern void FAdsEnableBanner(bool enable);

        [DllImport("__Internal")]
        private static extern void FAdsEnableInterstitial(bool enable);

        [DllImport("__Internal")]
        private static extern void FAdsInitializeConfig(string stringConfig, string rootKey, int strLength);

        [DllImport("__Internal")]
        private static extern int FAdsIsGDPRApplicable();

        [DllImport("__Internal")]
        private static extern bool FAdsIsNeedShowConsent();

        [DllImport("__Internal")]
        private static extern void FAdsGrantConsent();

        [DllImport("__Internal")]
        private static extern void FAdsBannerShow(string placement, string tag);

        [DllImport("__Internal")]
        private static extern void FAdsBannerHide();

        [DllImport("__Internal")]
        private static extern int FAdsInterstitialReadyState();

        [DllImport("__Internal")]
        private static extern void FAdsInterstitialShow(string placement, string tag);

        [DllImport("__Internal")]
        private static extern void FAdsEnableRewarded(string placement, bool enable);

        [DllImport("__Internal")]
        private static extern int FAdsRewardedReadyState(string adset);

        [DllImport("__Internal")]
        private static extern void FAdsRewardedShow(string adset, string placement, string tag);

        [DllImport("__Internal")]
        private static extern void FAdsSetCCPAOptOut(bool isRestricted);

        [DllImport("__Internal")]
        private static extern void FAdsSetCCPAApply(bool isCCPAApply);

        [DllImport("__Internal")]
        private static extern bool isTabletAdLayout();
        
        [DllImport("__Internal")]
        private static extern void FAdsAllowPauseRender(bool allow);

        #endregion

        #endregion
    }
#endif
}