﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using UnityEngine;

namespace FAdsSdk
{
    [SuppressMessage("ReSharper", "AccessToStaticMemberViaDerivedType")]
    public class FAdsNetwork : IConsentProvider
    {
        public const string SDK_VERSION = "1.5.0";

        /// <summary>
        /// Fired when event should be dispatched to analytics.
        /// </summary>
        public event Action<FAdsEvent> AdsEvent;

        /// <summary>
        /// Adjust.trackAdRevenue(AdjustConfig.AdjustAdRevenueSourceMopub, json);
        /// param - json encoded revenue 
        /// </summary>
        public event Action<string> AdjustRevenueImpression;

        public event Action ConsentInitialized;

        public string PolicyURL { get; private set; }
        public string VendorsURL { get; private set; }

        public event EventHandler RewardedComplete;

        private BannerPosition bannerPosition;

        private bool testMode;

        private bool inited = false;

        private const string cfgRootKey = "ads"; //json object containing config for fads sdk

        private string adsTag = string.Empty;

        public void Initialize(string cfg, bool limitTracking, bool ccpaApplies)
        {
            InternalInit(limitTracking, ccpaApplies);
            try
            {
                FAds.InitializeConfig(cfg, cfgRootKey);
            }
            catch (Exception e)
            {
                Debug.LogError("FAds fail to set config. " + e.Message);
            }
        }

        #region banner

        /// <summary>
        /// Setup banner
        /// </summary>
        /// <param name="bPos">banner position</param>
        /// <param name="safeAreaOffset">[0,1] - percent of height of current banner size</param>
        public void SetBannerPosition(BannerPosition bPos, float safeAreaOffset)
        {
            bannerPosition = bPos;
            FAds.SetBannerPosition(bannerPosition, safeAreaOffset);
        }

        public void ShowBanner(string placement)
        {
            FAds.ShowBanner(placement, adsTag);
        }

        public void HideBanner()
        {
            FAds.HideBanner();
        }

        public void UtilizeBanner()
        {
            FAds.HideBanner();
        }

        #endregion

        #region fs

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sendStateEvents"></param>
        /// <param name="placement"></param>
        /// <returns></returns>
        public bool HasInterstitial(bool sendStateEvents, string placement)
        {
            AdInterstitialReadyState state = (AdInterstitialReadyState) FAds.InterstitialReadyState();

            if (sendStateEvents)
            {
                switch (state)
                {
                    case AdInterstitialReadyState.Ready:
                        break;
                    case AdInterstitialReadyState.StateNotEnabled:
                        break;
                    case AdInterstitialReadyState.NotCached:
                        if (AdsEvent != null)
                            AdsEvent(new FAdsEvent("ad_interstitial_needed", CreateAdsNeededEventParams(placement),
                                EventAnalyticDestination.Everywhere));
                        break;
                    case AdInterstitialReadyState.SkipByInterDelay:
                        if (AdsEvent != null)
                            AdsEvent(new FAdsEvent("ad_interstitial_skipByInterDelay", CreateAdsNeededEventParams(placement),
                                EventAnalyticDestination.Everywhere));
                        break;
                    case AdInterstitialReadyState.SkipByRewardedDelay:
                        if (AdsEvent != null)
                            AdsEvent(new FAdsEvent("ad_interstitial_skipByRewardedDelay", CreateAdsNeededEventParams(placement),
                                EventAnalyticDestination.Everywhere));
                        break;
                }
            }

            return state == AdInterstitialReadyState.Ready;
        }

        public void ShowInterstitial(string placement)
        {
            FAds.ShowInterstitial(placement, adsTag);
        }

        #endregion

        #region reward

        [Obsolete("use EnableAds")]
        public void RequestRewardedVideo()
        {
            FAds.EnableAds(AdShowType.Rewarded);
        }

        public bool HasRewardedVideo(bool sendStateEvents, string placement)
        {
            AdRewardedReadyState state = (AdRewardedReadyState) FAds.RewardedReadyState();

            if (sendStateEvents)
            {
                if (state == AdRewardedReadyState.NotCached)
                {
                    if (AdsEvent != null)
                        AdsEvent(new FAdsEvent("ad_rewarded_needed", CreateAdsNeededEventParams(placement),
                            EventAnalyticDestination.Everywhere));
                }
            }

            return state == AdRewardedReadyState.Ready;
        }

        public void ShowRewardedVideo(string placement)
        {
            FAds.ShowRewarded(placement, adsTag);
        }

        private void OnRewardCompleted()
        {
            if (RewardedComplete != null)
                RewardedComplete.Invoke(null, EventArgs.Empty);
        }

        #endregion

        public void DisableAds(AdShowType showType)
        {
            FAds.DisableAds(showType);
        }

        public void EnableAds(AdShowType showType)
        {
            FAds.EnableAds(showType);
        }

        private void InternalInit(bool limitTracking, bool ccpaApplies)
        {
            if (inited)
                return;
            inited = true;
            FAdsManager.RewardedCompleted += OnRewardCompleted;
            FAdsManager.Initialized += OnInitialized;
            FAdsManager.AdsEventReceived += OnAdsEvent;
            FAdsManager.AdsImpressionRevenue += (json) =>
            {
                if (AdjustRevenueImpression != null) AdjustRevenueImpression(json);
            };

            FAds.InitializeSdk(limitTracking, ccpaApplies);
        }

        private void OnAdsEvent(FAdsEventData data)
        {
            if (data == null)
                return;

            var dict = data.EventDataToDictionary();
            var destination = ParseDestination(data);

            if (AdsEvent != null)
                AdsEvent.Invoke(new FAdsEvent(data.eventName, dict, destination));
        }

        private static EventAnalyticDestination ParseDestination(FAdsEventData data)
        {
            EventAnalyticDestination result;
            if (string.IsNullOrEmpty(data.service))
            {
                result = EventAnalyticDestination.Everywhere;
            }
            else if (data.service != null && data.service == "firebase")
            {
                result = EventAnalyticDestination.Firebase;
            }
            else if (data.service != null && data.service == "adjust_token")
            {
                result = EventAnalyticDestination.AdjustByToken;
            }
            else if (data.service != null && data.service == "adjust")
            {
                result = EventAnalyticDestination.Adjust;
            }
            else //fallback never should happen
            {
                result = EventAnalyticDestination.Firebase;
            }

            return result;
        }

        private void OnInitialized(FAdsInitData data)
        {
            PolicyURL = data != null ? data.privacyPolicyUrl : "";
            VendorsURL = data != null ? data.vendorListUrl : "";
            if (ConsentInitialized != null)
            {
                ConsentInitialized();
            }
        }

        #region consent

        public void InitForConsent(bool ccpaApplied, bool limitTracking)
        {
            InternalInit(limitTracking, ccpaApplied);
            FAds.DisableAds(AdShowType.Everything);
            FAds.InitializeConfig(null, null);
        }

        public void GrantConsent()
        {
            FAds.GrantConsent();
        }

        public bool ShouldShowConsentDialog
        {
            get { return FAds.ShouldShowConsentDialog(); }
        }

        public bool? IsGDPRApplicable
        {
            get
            {
                var value = FAds.IsGDPRApplicable();
                return value == 0 ? null : value > 0 ? (bool?) true : false;
            }
        }

        #endregion

        #region utils

        public string ConnectionType()
        {
            return FAds.GetConnectionType();
        }

        private Dictionary<string, string> CreateAdsNeededEventParams(string placement)
        {
            Dictionary<string, string> p = new Dictionary<string, string>()
            {
                {"connection", ConnectionType()},
                {"placement", placement}
            };
            return p;
        }

        public string GetStateLog()
        {
            return FAds.GetStateLog();
        }

        public bool IsTablet()
        {
            if (!inited)
                throw new Exception("FAds not inited");

            return FAds.IsTablet();
        }

        /// <summary>
        /// iOS feature only. pause unity engine when displaying any kind of overlay popup.
        /// *should be implemented in xcode
        /// </summary>
        /// <param name="allow"></param>
        public void AllowPauseRenderer(bool allow)
        {
            FAds.AllowPauseRenderer(allow);
        }


        #endregion
    }
}