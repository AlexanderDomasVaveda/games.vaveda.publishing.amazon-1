namespace FAdsSdk
{
    /// <summary>
    /// in which analytic system event should be dispatched
    /// </summary>
    public enum EventAnalyticDestination
    {
        Everywhere,
        Firebase,
        AdjustByToken,
        Adjust
    }
}