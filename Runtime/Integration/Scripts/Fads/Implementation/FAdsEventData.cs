using System;
using System.Collections.Generic;

namespace FAdsSdk
{
    [Serializable]
    public class FAdsEventData
    {
        public string eventName;
        public string service;
        public string[] eventKeys;
        public string[] eventVals;

        public Dictionary<string, string> EventDataToDictionary()
        {
            if (eventKeys == null || eventKeys.Length == 0 || eventVals == null || eventVals.Length == 0 ||
                eventKeys.Length != eventVals.Length)
            {
                return null;
            }

            try
            {
                Dictionary<string, string> result = new Dictionary<string, string>();
                for (int i = 0; i < eventKeys.Length; i++)
                {
                    result.Add(eventKeys[i], eventVals[i]);
                }

                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}